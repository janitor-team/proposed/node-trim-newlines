This is a stub types definition for trim-newlines (https://github.com/sindresorhus/trim-newlines).

trim-newlines provides its own type definitions, so you don't need @types/trim-newlines installed!